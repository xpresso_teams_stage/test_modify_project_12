""" This contains utiliity function to be used for running something
in the background or in separate threads """
import threading


def run_in_background(job_func, argument_list):
    """ Runs the provided job in a separate process as a background job
    Args:
        job_func(func): function which needs to be run as a background process
        argument_list(list): list of argument to be passed to the job_func
    """
    download_thread = threading.Thread(target=job_func,
                                       args=argument_list)
    download_thread.start()
